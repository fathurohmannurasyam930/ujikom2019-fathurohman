<?php
include"header.php";

include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Edit Jenis</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Edit Ruang
                                        </div>
                                        <div class="card-body card-block">
                                            <?php
                                                    include"database/koneksi.php";
                                                    $id=$_GET['id'];
                                                    $pilih=mysqli_query($koneksi, "SELECT * FROM petugas WHERE id='$id'");
                                                    $tampil=mysqli_fetch_array($pilih);
                                            ?>
                                            <form action="" method="post" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class="form-control-label">Kode Petugas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                                                    <input type="text" name="kode_petugas" class="form-control" value="<?php echo $tampil['kode_petugas'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Petugas</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_petugas" class="form-control" value="<?php echo $tampil['nama_petugas'];?>" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Username</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="username" class="form-control" value="<?php echo $tampil['username'];?>" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Password</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="password" name="password" class="form-control" value="<?php echo $tampil['password'];?>" required>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Level</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                    <input type="text" name="level" class="form-control" value="<?php echo $tampil['level'];?>" required>
                                                    </div>
                                                </div>


                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" name="edit" value="edit">
                                                </div>
                                            </form>
                                           <?php
                                            include"database/koneksi.php";
                                            if(isset($_POST['edit'])){
                                                $id=$_POST['id'];
                                                $kode_petugas=$_POST['kode_petugas'];
                                                $nama_petugas=$_POST['nama_petugas'];
                                                $username=$_POST['username'];
                                                $password=$_POST['password'];
                                                $level=$_POST['level'];



                                                $input=mysqli_query($koneksi, "UPDATE petugas SET kode_petugas='$kode_petugas', nama_petugas='$nama_petugas', username='$username', password='$password', level='$level' WHERE id='$id'");

                                                if ($input) {
                                                    echo "Berhasil";
                                                    ?>
                                                    <script type="text/javascript">
                                                        window.location.href="pengguna.php";
                                                    </script>
                                                    <?php
                                                }else{
                                                    echo"gagal";
                                                }
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>