<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
<header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Peminjaman</h3>
                        </div>
                    </div> 
                </div>
            </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                        <div class="row" align="center">
                          <div class="col-lg-12">
                                          <div class="au-card recent-report">
                                             <h2>Pinjaman Barang</h2>
                                             <span>Personal</span>
                                             <hr>
                                             <a href="pinjam.php?kode_db=<?php echo $kode ?>&kode_pinjam=<?php echo $kd ?>"><button class="btn btn-info" type="button">Pinjam</button></a>
                                          </div>
                          </div> 
                       </div>

                        
                       

                     <div class="row">
                   
                    <div class="col-md-12">
                                     <div class="au-card recent-report">
                                    <center><h4>Data Peminjaman</h4>
                                     <br>
                                     <a href="gp.php">
                                    <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class=""></i>Data Detail Peminjaman</button>
                                    </a>
                                    </center>
                                   
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-data2" id="dataTables">
                                                        <thead>
                                                            <tr>
                                                                <th>Kode Peminjam</th>
                                                                <th>Nama Barang</th>
                                                                <th>Tanggal Pinjam</th>
                                                                <th>Tanggal kembali</th>
                                                                <th>Status</th>
                                                                <th>Pegawai</th>
                                                                <th>Kelas</th>
                                                                <th>Opsi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <?php
                                                                foreach($db->peminjam() as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $x['kode_peminjaman'];?></td>
                                                                    <td><?php echo $x['kode_inventaris'];?></td>
                                                                    <td><?php echo $x['tanggal_pinjam']; ?></td>
                                                                    <td>
                                                                      <a href="pro_inven.php?kode_peminjaman=<?php echo $x['kode_peminjaman'];?>&aksi=kembali">
                                                                        <button type="reset" class="btn btn-primary btn-sm">
                                                                            <i class=""></i> kembali
                                                                        </button>
                                                                        </a>
                                                                    </td>
                                                                    <td><?php echo $x['status_peminjaman']; ?></td>
                                                                    <td><?php echo $x['kode_pegawai']; ?></td>
                                                                    <td><?php echo $x['kelas']; ?></td>
                                                                    <td>
                                                                        <a href="">
                                                                        <button type="reset" class="btn btn-primary btn-sm">
                                                                            <i class=""></i> Detail
                                                                        </button>
                                                                        </a>
                                                                        <a href="pro_inven.php?kode_peminjaman=<?php echo $x['kode_peminjaman']; ?>&aksi=hapus_pinjam">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                     
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>
            </div>

           

	            <?php
	            include"footer.php";
	            ?>
	            
        </div>
    </div>

    
</body>
</html>
<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>