<?php
include"header.php";

include 'database/class.php';
$db = new database();
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Tambah Data Pegawai / USER </h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

               

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Form</strong> Tambah Pegawai / USER
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="pro_inven.php?aksi=tambah_pg" method="post" class="form-horizontal">
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Kode Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="kode_pegawai" placeholder="Kode Pegawai" class="form-control" >
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">NIP</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nip" placeholder="NIP" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Nama Pegawai</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="nama_pegawai" placeholder="Nama Pegawai" class="form-control" required>
                                                    </div>
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Usernama/Email</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="username" placeholder="Usernama / Email" class="form-control" required>
                                                    </div>
                                                </div>
                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="select" class=" form-control-label">Password</label>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <input type="text" name="password" placeholder="Password" class="form-control" required>
                                                    </div>
                                                </div>
                                                

                                                <div class="card-footer">
                                                <input class="btn btn-primary btn-sm" type="submit" value="Simpan" name="simpan">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>
