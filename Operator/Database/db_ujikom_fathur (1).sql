-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21 Feb 2019 pada 07.09
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ujikom_fathur`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `asal_barang`
--

CREATE TABLE `asal_barang` (
  `id` int(15) NOT NULL,
  `asal_barang` varchar(255) NOT NULL,
  `kode_asalbarang` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `asal_barang`
--

INSERT INTO `asal_barang` (`id`, `asal_barang`, `kode_asalbarang`) VALUES
(1, 'Animasi', 'KAB01'),
(2, 'Rekayasa Perangkat lunak', 'KAB02'),
(3, 'Brocasting', 'KAB03'),
(4, 'Teknik Kendaran Ringan', 'KAB04'),
(5, 'Teknik Pengelasan', 'KAB05'),
(6, 'TU', 'KAB06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL,
  `kode_inventaris` varchar(200) NOT NULL,
  `jumlah` int(15) NOT NULL,
  `kode_peminjaman` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `kode_inventaris`, `jumlah`, `kode_peminjaman`) VALUES
(1, 'KI01', 1, 'KDP01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `kode_inventaris` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `asal_barang` varchar(200) NOT NULL,
  `kondisi` varchar(200) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `ruang` varchar(20) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `petugas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`kode_inventaris`, `nama_barang`, `asal_barang`, `kondisi`, `jumlah`, `jenis`, `ruang`, `tanggal_register`, `keterangan`, `petugas`) VALUES
('KI01', 'Laptop', 'Animasi', 'Rusak', 10, 'Hp', 'Lab Animasi', '2019-02-20 10:29:00', 'Baik', 'Fathurohman Nurasaym');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL,
  `nama_jenis` varchar(200) NOT NULL,
  `kode_jenis` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Lenovo', '0KJ1', 'rpl,anm'),
(2, 'Hp', '0KJ2', 'rpl,anm'),
(3, 'Infokus', '0KJ3', 'diTU'),
(4, 'mebel', '0KJ4', 'gudang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `kode_level` varchar(200) NOT NULL,
  `nama_level` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`kode_level`, `nama_level`) VALUES
('1', ''),
('2', ''),
('3', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `kode_pegawai` varchar(11) NOT NULL,
  `nip` char(15) NOT NULL,
  `nama_pegawai` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`kode_pegawai`, `nip`, `nama_pegawai`, `username`, `password`) VALUES
('1', '', '', '', '123'),
('2', '', '', '', '123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `kode_peminjaman` varchar(15) NOT NULL,
  `kelas` varchar(200) NOT NULL,
  `kode_inventaris` varchar(255) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(200) NOT NULL,
  `kode_pegawai` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`kode_peminjaman`, `kelas`, `kode_inventaris`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `kode_pegawai`) VALUES
('KDP01', '', 'KI1', '2019-01-16 05:28:32', '2019-01-16 00:00:00', 'Belum Dikembalikan', 'Fathurohman ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `kode_petugas` varchar(15) NOT NULL,
  `nama_petugas` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`kode_petugas`, `nama_petugas`, `username`, `password`, `level`) VALUES
('1', 'Fathurohman Nurasaym', 'oprator@username.com', '123', 'Operator'),
('2', 'M Raja', 'user@username.com', '123', 'User');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL,
  `nama_ruang` varchar(200) NOT NULL,
  `kode_ruang` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab Animasi', '0A1', 'Rapih');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asal_barang`
--
ALTER TABLE `asal_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD UNIQUE KEY `id_detail_pinjam` (`id_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD UNIQUE KEY `kode_inventaris` (`kode_inventaris`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`),
  ADD UNIQUE KEY `id_jenis` (`id_jenis`),
  ADD UNIQUE KEY `kode_jenis` (`kode_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`kode_level`),
  ADD UNIQUE KEY `id_level` (`kode_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`kode_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`kode_peminjaman`),
  ADD UNIQUE KEY `id_peminjaman` (`kode_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD UNIQUE KEY `id_petugas` (`kode_petugas`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`),
  ADD UNIQUE KEY `id_ruang` (`id_ruang`),
  ADD UNIQUE KEY `kode_ruang` (`kode_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asal_barang`
--
ALTER TABLE `asal_barang`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
