<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Fontfaces CSS-->
    <link href="login/css/font-face.css" rel="stylesheet" media="all">
    <link href="login/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="login/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="login/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="login/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="login/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="login/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="login/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="login/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="login/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="login/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="login/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="login/css/theme.css" rel="stylesheet" media="all">

</head>
<style type="text/css">
body {
    background-image: url("login/images/01.jpg");
    background-size: cover; 
}
    
</style>

<body class="animsition">
    <div class="page-wrapper">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                           <h3>Peminjaman Inventori Sekolah</h3>
                        </div>
                        <div class="login-form">
                            <form action="aksi_login.php" method="post">
                                <div class="form-group">
                                    <label align="center">NIP SISWA</label>
                                    <input class="au-input au-input--full" type="text" name="nip" placeholder="Nip Siswa">
                                </div>
                                <button class="au-btn au-btn--block au-btn--blue m-b-20" type="submit" value="Login" name="login">Masuk</button>
                                
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>

    </div>

    <!-- Jquery JS-->
    <script src="login/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="login/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="login/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="login/vendor/slick/slick.min.js">
    </script>
    <script src="login/vendor/wow/wow.min.js"></script>
    <script src="login/vendor/animsition/animsition.min.js"></script>
    <script src="login/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="login/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="login/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="login/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="login/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="login/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="login/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="login/js/main.js"></script>

</body>

</html>
<!-- end document-->